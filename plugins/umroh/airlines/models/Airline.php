<?php namespace Umroh\Airlines\Models;

use Model;

/**
 * Model
 */
class Airline extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * Set Database Connection
     */
    protected $connection = 'mysql_package';


    /**
     * @var string The database table used by the model.
     */
    public $table = 'airlines';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'packages' => ['umroh\Packages\Models\Package','key', 'package_id','otherKey' => 'id']
    ];
    public $belongsTo = [];
    public $belongsToMany = [
        'packageFlights' => ['umroh\Packages\Models\PackageFlights', 'key' => 'airline_id', 'otherKey' => 'id']
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
