<?php namespace Umroh\Packagefacility\Models;

use Model;

/**
 * Model
 */
class PackageFacility extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * Set Database Connection
     */
    protected $connection = 'mysql_package';

    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'package_facilities';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'partner'               => 'required',
        'name'                  => 'required',
        'facility_include'      => 'required',
        'facility_exclude'      => 'required',
        'terms_and_conditions'  => 'required',
        'travel_details'        => 'required',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'partner' => ['umroh\Partners\Models\Partner', 'key' => 'partner_id', 'otherKey' => 'id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
