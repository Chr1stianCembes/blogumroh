<?php namespace Umroh\Packagefacility\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmrohPackagefacilityTable2 extends Migration
{
    public function up()
    {
        Schema::create('umroh_packagefacility_table', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umroh_packagefacility_table');
    }
}
