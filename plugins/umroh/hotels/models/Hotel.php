<?php namespace Umroh\Hotels\Models;

use Model;

/**
 * Model
 */
class Hotel extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;
    
    protected $connection = 'mysql_package';


    /**
     * @var string The database table used by the model.
     */
    public $table = 'hotels';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'country' => 'required',
        'city' => 'required',
        'address' => 'required',
        'zip' => 'required',
        'latitude' => 'required',
        'longitude' => 'required',
        'score' => 'required',
        'rating' => 'required',
        'total_rooms' => 'required',
        'currency_code' => 'required',
        'country_code' => 'required',
        'timezone' => 'required',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'packages' => ['umroh\Packages\Models\Package','key', 'package_id','otherKey' => 'id']
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
