<?php namespace Umroh\Hotels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmrohHotelsTable extends Migration
{
    public function up()
    {
        Schema::create('umroh_hotels_table', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umroh_hotels_table');
    }
}
