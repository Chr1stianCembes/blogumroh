<?php namespace Umroh\Banks\Models;

use Model;

/**
 * Model
 */
class Bank extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * Set Database Connection
     */
    protected $connection = 'mysql_core';


    /**
     * @var string The database table used by the model.
     */
    public $table = 'banks';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'partner' => ['umroh\Partners\Models\Partner', 'key' => 'bank_id', 'otherKey' => 'id'],
        'savingWithdrawDetail' => ['umroh\Savings\Models\SavingWithdrawDetail', 'key' => 'bank_id', 'otherKey' => 'id'],
    ];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
